package com.kostya.ringtesttask;

import com.kostya.ringtesttask.data.source.NewsMemorySource;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class NewsMemorySourceTest {

    private NewsFactory newsFactory;

    @Before
    public void setup() {
        newsFactory = new NewsFactory();
    }

    @Test
    public void shouldStoreNewsInMemorySource() {
        NewsMemorySource source = new NewsMemorySource();
        source.add(newsFactory.getNews(2));
        source.getNews()
                .test()
                .assertSubscribed()
                .assertValue(list -> list.size() == 2);
    }

    @Test
    public void shouldHaveAvailableNews() {
        int limit = 10;
        int page = 3;
        NewsMemorySource source = new NewsMemorySource();
        source.add(newsFactory.getNews(limit * page));
        assertThat(source.areNewsAvailable(limit, page)).isEqualTo(true);
    }

    @Test
    public void shouldNotHaveAvailableNews() {
        int limit = 10;
        int page = 3;
        NewsMemorySource source = new NewsMemorySource();
        source.add(newsFactory.getNews(limit * page));
        assertThat(source.areNewsAvailable(limit, 5)).isEqualTo(false);
    }

    @Test
    public void shouldClearMemorySource() {
        NewsMemorySource source = new NewsMemorySource();
        source.add(newsFactory.getNews(2));
        source.getNews()
                .test()
                .assertSubscribed()
                .assertValue(list -> list.size() == 2);

        source.clear();

        source.getNews()
                .test()
                .assertSubscribed()
                .assertValue(list -> list.isEmpty());
    }
}
