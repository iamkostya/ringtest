package com.kostya.ringtesttask;

import com.google.gson.Gson;
import com.kostya.ringtesttask.data.entity.Child;
import com.kostya.ringtesttask.data.entity.NewsItem;
import com.kostya.ringtesttask.data.entity.RedditResponse;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NewsFactory {

    private List<NewsItem> news = new ArrayList<>();

    public NewsFactory() {
        File file = new File(getClass().getClassLoader().getResource("top.json").getPath());
        String json = null;
        try {
            json = readBytes(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Gson gson = new Gson();
        RedditResponse response = gson.fromJson(json, RedditResponse.class);
        for (Child child : response.getData().getChildren()) {
            news.add(child.getNewsItem());
        }
    }

    public List<NewsItem> getNews() {
        return news;
    }

    public List<NewsItem> getNews(int count) {
        return news.subList(0, count);
    }

    private String readBytes(File file) throws IOException {
        try(BufferedReader br = new BufferedReader(new FileReader(file))) {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            return sb.toString();
        }
    }
}
