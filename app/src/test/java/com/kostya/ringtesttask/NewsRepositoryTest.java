package com.kostya.ringtesttask;

import com.kostya.ringtesttask.data.source.NewsMemorySource;
import com.kostya.ringtesttask.data.source.NewsNetworkSource;
import com.kostya.ringtesttask.data.NewsRepository;
import com.kostya.ringtesttask.data.entity.NewsItem;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.powermock.api.support.membermodification.MemberModifier;

import java.util.List;

import io.reactivex.Single;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;

public class NewsRepositoryTest {

    private NewsFactory newsFactory;

    @Before
    public void setup() {
        newsFactory = new NewsFactory();
    }

    @Test
    public void shouldReturnCachedNews() {
        NewsMemorySource memorySource = Mockito.mock(NewsMemorySource.class);
        NewsNetworkSource networkSource = Mockito.mock(NewsNetworkSource.class);
        List<NewsItem> testList = newsFactory.getNews(10);
        Mockito.when(memorySource.getNews()).thenReturn(Single.just(testList));

        NewsRepository newsRepository = new NewsRepository(networkSource, memorySource);

        newsRepository.getCachedNews()
                .test()
                .assertSubscribed()
                .assertNoErrors()
                .assertComplete()
                .assertValue(list -> list.equals(testList))
                .assertValue(list -> list.size() == 10);
    }

    @Test
    public void shouldLoadFromNetworkOnRefresh() {
        NewsMemorySource memorySource = Mockito.mock(NewsMemorySource.class);
        NewsNetworkSource networkSource = Mockito.mock(NewsNetworkSource.class);
        List<NewsItem> testList = newsFactory.getNews(10);
        Mockito.when(networkSource.getNews(10, null)).thenReturn(Single.just(testList));

        NewsRepository newsRepository = new NewsRepository(networkSource, memorySource);

        newsRepository.loadMoreNews(true).test();
        verify(networkSource).getNews(10, null);
    }

    @Test
    public void shouldReturnErrorForBiggerPage() throws IllegalAccessException {
        NewsMemorySource memorySource = Mockito.mock(NewsMemorySource.class);
        NewsNetworkSource networkSource = Mockito.mock(NewsNetworkSource.class);
        NewsRepository newsRepository = new NewsRepository(networkSource, memorySource);

        MemberModifier.field(NewsRepository.class, "currentPage").set(newsRepository , 5);

        newsRepository.loadMoreNews(false)
                .test()
                .assertSubscribed()
                .assertErrorMessage("No more news available");
    }

    @Test
    public void shouldReturnNewsFromMemoryCache() throws IllegalAccessException {
        NewsMemorySource memorySource = new NewsMemorySource();
        NewsNetworkSource networkSource = Mockito.mock(NewsNetworkSource.class);
        List<NewsItem> cachedNews = newsFactory.getNews(30);
        memorySource.add(cachedNews);

        NewsRepository newsRepository = new NewsRepository(networkSource, memorySource);

        MemberModifier.field(NewsRepository.class, "lastItemId").set(newsRepository , cachedNews.get(20).getName());

        newsRepository.loadMoreNews(false)
                .test()
                .assertSubscribed()
                .assertNoErrors()
                .assertComplete()
                .assertValue(list -> list.size() == 20);
    }

    @Test
    public void shouldLoadNewsFromNetwork() {
        NewsMemorySource memorySource = Mockito.mock(NewsMemorySource.class);
        NewsNetworkSource networkSource = Mockito.mock(NewsNetworkSource.class);
        List<NewsItem> testList = newsFactory.getNews(10);
        Mockito.when(networkSource.getNews(10, null)).thenReturn(Single.just(testList));
        Mockito.when(memorySource.areNewsAvailable(anyInt(), anyInt())).thenReturn(false);

        NewsRepository newsRepository = new NewsRepository(networkSource, memorySource);

        newsRepository.loadMoreNews(false)
                .test()
                .assertSubscribed()
                .assertNoErrors()
                .assertComplete()
                .assertValue(list -> list.size() == 10);

        verify(networkSource).getNews(10, null);
    }

}
