package com.kostya.ringtesttask.data.entity;

public class Image {

    private Source source;
    private Variants variants;

    public Source getSources() {
        return source;
    }

    public Variants getVariants() {
        return variants;
    }
}
