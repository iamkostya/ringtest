package com.kostya.ringtesttask.data.source;

import com.kostya.ringtesttask.data.entity.NewsItem;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;

public class NewsMemorySource implements NewsSourceContract {

    private List<NewsItem> news = new ArrayList<>();

    @Inject
    public NewsMemorySource() {
    }

    @Override
    public Single<List<NewsItem>> getNews(int limit, String after) {
        return Observable.fromIterable(news)
                .takeWhile(it -> !it.getName().equals(after))
                .toList();
    }

    /**
     * @return All available in cache news
     */
    public Single<List<NewsItem>> getNews() {
        return Single.just(news);
    }

    /**
     * Method to check if specified number of news are available the cache
     * @param limit news per page
     * @param page page number
     * @return Boolean that indicates whether or not the cache contains specified number of news
     */
    public boolean areNewsAvailable(int limit, int page) {
        return news.size() >= limit * page;
    }

    public void add(List<NewsItem> news) {
        this.news.addAll(news);
    }

    public void clear() {
        news.clear();
    }
}
