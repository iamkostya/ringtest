package com.kostya.ringtesttask.data.entity;

import java.util.List;

public class Data {

    private List<Child> children;
    private String after;

    public List<Child> getChildren() {
        return children;
    }

    public String getAfter() {
        return after;
    }
}
