package com.kostya.ringtesttask.data.api;

import com.kostya.ringtesttask.data.entity.RedditResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface API {
    @GET("top.json")
    Observable<RedditResponse> getNews(@Query("limit") int limit, @Query("after") String after);
}
