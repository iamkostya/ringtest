package com.kostya.ringtesttask.data.entity;

import com.google.gson.annotations.SerializedName;

public class NewsItem {

    private String name;
    private String author;
    private String thumbnail;
    private String title;
    @SerializedName("num_comments")
    private Integer comments;
    @SerializedName("created_utc")
    private Long created;
    private Preview preview;

    public String getAuthor() {
        return author;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getTitle() {
        return title;
    }

    public Integer getComments() {
        return comments;
    }

    public Long getCreated() {
        return created;
    }

    public Preview getPreview() {
        return preview;
    }

    public String getName() {
        return name;
    }
}
