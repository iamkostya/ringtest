package com.kostya.ringtesttask.data.source;

import com.kostya.ringtesttask.data.entity.NewsItem;

import java.util.List;

import io.reactivex.Single;

/**
 * Interface for all news sources like Network, DB, Memory
 */
public interface NewsSourceContract {

    /**
     * Returns Single of List<NewsItem> for given parameters
     * @param limit How much news to return
     * @param after ID of the last NewsItem for searching in ordered list
     * @return List of news for specified parameters
     */
    Single<List<NewsItem>> getNews(int limit, String after);

}
