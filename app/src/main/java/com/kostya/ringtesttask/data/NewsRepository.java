package com.kostya.ringtesttask.data;

import com.kostya.ringtesttask.data.entity.NewsItem;
import com.kostya.ringtesttask.data.source.NewsMemorySource;
import com.kostya.ringtesttask.data.source.NewsNetworkSource;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class NewsRepository implements NewsRepositoryContract {

    private static final int MAX_PAGES = 5;
    private static final int NEWS_PER_PAGE = 10;

    private int currentPage = 1;
    private String lastItemId;

    private NewsNetworkSource newsNetworkSource;
    private NewsMemorySource newsMemorySource;

    @Inject
    public NewsRepository(NewsNetworkSource newsNetworkSource, NewsMemorySource newsMemorySource) {
        this.newsNetworkSource = newsNetworkSource;
        this.newsMemorySource = newsMemorySource;
    }

    @Override
    public Single<List<NewsItem>> loadMoreNews(boolean refresh) {
        if (refresh) {
            newsMemorySource.clear();
            currentPage = 1;
            lastItemId = null;
            return loadFromNetwork(NEWS_PER_PAGE, lastItemId);
        } else {
            if (currentPage >= MAX_PAGES) {
                return Single.error(new Throwable("No more news available"));
            }
            currentPage++;

            if (newsMemorySource.areNewsAvailable(NEWS_PER_PAGE, currentPage)) {
                return newsMemorySource.getNews(NEWS_PER_PAGE, lastItemId);
            } else {
                return loadFromNetwork(NEWS_PER_PAGE, lastItemId);
            }
        }
    }

    @Override
    public Single<List<NewsItem>> getCachedNews() {
        return newsMemorySource.getNews();
    }

    private Single<List<NewsItem>> loadFromNetwork(int limit, String after) {
        return newsNetworkSource.getNews(limit, after)
                .doOnSuccess(list -> newsMemorySource.add(list))
                .doOnSuccess(list -> lastItemId = list.get(list.size() - 1).getName());
    }
}
