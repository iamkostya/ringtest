package com.kostya.ringtesttask.data.entity;

import com.google.gson.annotations.SerializedName;

public class Child {

    @SerializedName("data")
    private NewsItem newsItem;

    public NewsItem getNewsItem() {
        return newsItem;
    }
}
