package com.kostya.ringtesttask.data.source;

import com.kostya.ringtesttask.data.api.API;
import com.kostya.ringtesttask.data.entity.Child;
import com.kostya.ringtesttask.data.entity.NewsItem;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class NewsNetworkSource implements NewsSourceContract {

    private API api;

    @Inject
    public NewsNetworkSource(API api) {
        this.api = api;
    }

    @Override
    public Single<List<NewsItem>> getNews(int limit, String after) {
        return api.getNews(limit, after)
                .concatMapIterable(it -> it.getData().getChildren())
                .map(Child::getNewsItem)
                .toList();
    }
}
