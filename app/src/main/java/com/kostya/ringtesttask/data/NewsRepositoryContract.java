package com.kostya.ringtesttask.data;

import com.kostya.ringtesttask.data.entity.NewsItem;

import java.util.List;

import io.reactivex.Single;

public interface NewsRepositoryContract {
    /**
     * Method to load next portion of news
     * @param refresh If this parameter is true this indicated the called wants to wipe ay cached
     *                news and load fresh data
     * @return Single list of NewsItem
     */
    Single<List<NewsItem>> loadMoreNews(boolean refresh);

    /**
     * @return Specifically cached news if any exist
     */
    Single<List<NewsItem>> getCachedNews();
}
