package com.kostya.ringtesttask;

import android.content.Context;
import android.util.Pair;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;

import java.util.Locale;

public final class Utils {

    private Utils() {
    }

    public static String getPrettyDate(Context context, DateTime dateTime) {
        int count;
        DateTime now = DateTime.now(dateTime.getZone());
        count = Days.daysBetween(dateTime, now).getDays();
        if (count == 0) {
            count = Hours.hoursBetween(dateTime, now).getHours();
            if (count == 0) {
                count = Minutes.minutesBetween(dateTime, now).getMinutes();
                if (count == 0) {
                    return context.getString(R.string.just_now);
                } else {
                    return String.format(context.getResources().getQuantityString(R.plurals.minutes_template, count), count);
                }
            } else {
                return String.format(context.getResources().getQuantityString(R.plurals.hours_template, count), count);
            }
        } else {
            return String.format(context.getResources().getQuantityString(R.plurals.days_template, count), count);
        }
    }

}
