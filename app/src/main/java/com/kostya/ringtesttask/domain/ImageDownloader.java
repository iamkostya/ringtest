package com.kostya.ringtesttask.domain;

import android.app.DownloadManager;
import android.net.Uri;
import android.os.Environment;
import android.webkit.URLUtil;

import java.util.Random;

import javax.inject.Inject;

/**
 * Simple wrapper around Android's DownloadManager to gracefully handle downloading images from the network.
 * For a given URL downloads file into PICTURES folder on a device.
 */
public class ImageDownloader {

    private DownloadManager downloadManager;
    private Random random;

    @Inject
    public ImageDownloader(DownloadManager downloadManager) {
        this.downloadManager = downloadManager;
        this.random = new Random();
    }

    public void download(String imageUrl) {
        if (!URLUtil.isValidUrl(imageUrl)) throw new IllegalArgumentException("Provided URL is not valid");
        Uri Download_Uri = Uri.parse(imageUrl);
        DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setTitle("Image has been downloaded to your gallery");
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_PICTURES, generateImageName());
        downloadManager.enqueue(request);
    }

    private String generateImageName() {
        return random.nextLong() + ".png";
    }
}
