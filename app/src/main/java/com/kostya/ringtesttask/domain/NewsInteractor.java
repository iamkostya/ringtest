package com.kostya.ringtesttask.domain;

import com.kostya.ringtesttask.data.NewsRepositoryContract;
import com.kostya.ringtesttask.data.entity.NewsItem;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class NewsInteractor {

    private NewsRepositoryContract newsRepository;

    @Inject
    public NewsInteractor(NewsRepositoryContract newsRepository) {
        this.newsRepository = newsRepository;
    }

    /**
     * Proxy for Repository's loadMoreNews method.
     */
    public Single<List<NewsItem>> loadMoreNews(boolean refresh) {
        return newsRepository.loadMoreNews(refresh);
    }

    /**
     * Proxy for Repository's getCachedNews() method.
     */
    public Single<List<NewsItem>> getCachedNews() {
        return newsRepository.getCachedNews();
    }
}
