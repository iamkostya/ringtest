package com.kostya.ringtesttask.di;

import com.kostya.ringtesttask.ui.list.NewsActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = DataComponent.class)
public interface NewsComponent {
    void inject(NewsActivity view);
}
