package com.kostya.ringtesttask.di;

import android.app.DownloadManager;
import android.content.Context;

import com.kostya.ringtesttask.data.NewsRepository;
import com.kostya.ringtesttask.data.NewsRepositoryContract;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = { DataModule.class })
public interface DataComponent {
    NewsRepositoryContract injectNewsRepository();
    DownloadManager injectDownloadManager();
}
