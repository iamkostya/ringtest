package com.kostya.ringtesttask.di;

import com.kostya.ringtesttask.ui.image.ImageViewActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = DataComponent.class)
public interface ImageViewComponent {
    void inject(ImageViewActivity view);
}
