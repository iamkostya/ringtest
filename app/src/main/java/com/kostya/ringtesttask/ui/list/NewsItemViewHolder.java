package com.kostya.ringtesttask.ui.list;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.request.RequestOptions;
import com.kostya.ringtesttask.R;
import com.kostya.ringtesttask.Utils;
import com.kostya.ringtesttask.data.entity.NewsItem;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.net.URL;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.subjects.PublishSubject;

public class NewsItemViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.thumbnail)
    protected ImageView thumbnail;
    @BindView(R.id.title)
    protected TextView title;
    @BindView(R.id.author)
    protected TextView author;
    @BindView(R.id.comments)
    protected TextView comments;
    @BindView(R.id.time)
    protected TextView time;

    public NewsItemViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    void bind(NewsItem newsItem, PublishSubject<NewsItem> clickSubject) {
        if (URLUtil.isValidUrl(newsItem.getThumbnail())) {
            Glide.with(itemView)
                    .load(newsItem.getThumbnail())
                    .into(thumbnail);
            thumbnail.setOnClickListener((v) -> clickSubject.onNext(newsItem));
        } else {
            Glide.with(itemView)
                    .load(R.drawable.ic_placeholder)
                    .into(thumbnail);
        }
        title.setText(newsItem.getTitle());
        author.setText(String.format(itemView.getContext().getString(R.string.author_template), newsItem.getAuthor()));
        comments.setText(String.format(itemView.getResources().getQuantityString(R.plurals.comments_template, newsItem.getComments()), newsItem.getComments()));
        time.setText(Utils.getPrettyDate(itemView.getContext(), new DateTime(newsItem.getCreated() * 1000, DateTimeZone.UTC)));
    }

}
