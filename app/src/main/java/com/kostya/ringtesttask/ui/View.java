package com.kostya.ringtesttask.ui;

import android.support.annotation.Nullable;

public interface View {
    void setLoading(boolean loading);
    void showError(@Nullable String message);
}
