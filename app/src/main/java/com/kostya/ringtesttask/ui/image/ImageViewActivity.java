package com.kostya.ringtesttask.ui.image;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.jakewharton.rxbinding2.view.RxView;
import com.kostya.ringtesttask.App;
import com.kostya.ringtesttask.R;
import com.kostya.ringtesttask.di.DaggerImageViewComponent;
import com.kostya.ringtesttask.di.DaggerNewsComponent;
import com.kostya.ringtesttask.ui.BaseActivity;
import com.tbruyelle.rxpermissions2.RxPermissions;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class ImageViewActivity extends BaseActivity implements ImageViewContract {

    private static final String URL_EXTRA_KEY = "image_url";

    @BindView(R.id.root)
    protected CoordinatorLayout coordinatorLayout;
    @BindView(R.id.image)
    protected ImageView imageView;
    @BindView(R.id.download)
    protected FloatingActionButton downloadButton;
    @BindView(R.id.loader)
    protected ProgressBar progressBar;

    @Inject
    protected ImagePresenter presenter;

    private String imageUrl;
    private PublishSubject<ImageLoadingEvent> imageLoadingSubject = PublishSubject.create();

    @Override
    protected int getLayout() {
        return R.layout.activity_image_view;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerImageViewComponent.builder()
                .dataComponent((((App) getApplication())).getDataComponent())
                .build()
                .inject(this);
        if (!getIntent().hasExtra(URL_EXTRA_KEY)) {
            throw new IllegalStateException("No image url provided");
        }
        imageUrl = getIntent().getStringExtra(URL_EXTRA_KEY);
        Glide.with(this).load(imageUrl).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                downloadButton.setVisibility(View.GONE);
                imageLoadingSubject.onNext(ImageLoadingEvent.FAILED);
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                imageLoadingSubject.onNext(ImageLoadingEvent.FINISHED);
                return false;
            }
        }).into(imageView);

        presenter.bindView(this);
    }

    @Override
    public Observable<String> saveEvent() {
        return RxView.clicks(downloadButton)
                .compose(new RxPermissions(this).ensure(Manifest.permission.WRITE_EXTERNAL_STORAGE))
                .filter(granted -> {
                    if (granted) {
                        Snackbar.make(coordinatorLayout, getString(R.string.saving_image), Snackbar.LENGTH_LONG).show();
                    } else {
                        Snackbar.make(coordinatorLayout, getString(R.string.no_permission_granted), Snackbar.LENGTH_LONG).show();
                    }
                    return granted;
                })
                .map(it -> imageUrl);
    }

    @Override
    public Observable<ImageLoadingEvent> imageLoadingEvent() {
        return imageLoadingSubject;
    }

    @Override
    public void setLoading(boolean loading) {
        progressBar.setVisibility(loading ? View.VISIBLE : View.GONE);
        downloadButton.setVisibility(loading ? View.GONE : View.VISIBLE);
    }

    @Override
    public void showError(String message) {
        if (message == null) message = getString(R.string.plain_error);
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        presenter.destroyView();
        super.onDestroy();
    }

    public static void start(Context context, String url) {
        Intent starter = new Intent(context, ImageViewActivity.class);
        starter.putExtra(URL_EXTRA_KEY, url);
        context.startActivity(starter);
    }
}
