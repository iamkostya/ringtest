package com.kostya.ringtesttask.ui.image;

import com.kostya.ringtesttask.ui.View;

import io.reactivex.Observable;

public interface ImageViewContract extends View {

    Observable<String> saveEvent();
    Observable<ImageLoadingEvent> imageLoadingEvent();

    enum ImageLoadingEvent {
        FINISHED, FAILED
    }
}
