package com.kostya.ringtesttask.ui.list;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.jakewharton.rxbinding2.support.v4.widget.RxSwipeRefreshLayout;
import com.jakewharton.rxbinding2.support.v7.widget.RecyclerViewScrollEvent;
import com.jakewharton.rxbinding2.support.v7.widget.RxRecyclerView;
import com.kostya.ringtesttask.App;
import com.kostya.ringtesttask.R;
import com.kostya.ringtesttask.data.entity.NewsItem;
import com.kostya.ringtesttask.di.DaggerNewsComponent;
import com.kostya.ringtesttask.ui.BaseActivity;
import com.kostya.ringtesttask.ui.image.ImageViewActivity;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class NewsActivity extends BaseActivity implements NewsView {

    private static final String KEY_LIST_SCROLL_STATE = "list_state";
    private static final int LOADING_THRESHOLD = 3;

    @BindView(R.id.root_layout)
    protected CoordinatorLayout coordinatorLayout;
    @BindView(R.id.refresh_layout)
    protected SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.news_list)
    protected RecyclerView recyclerView;

    @Inject
    protected NewsPresenter presenter;

    private NewsAdapter adapter = new NewsAdapter();
    private Parcelable listScrollState;
    private int previousTotalItems = 0;

    @Override
    protected int getLayout() {
        return R.layout.activity_news_list;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerNewsComponent.builder()
                .dataComponent((((App) getApplication())).getDataComponent())
                .build()
                .inject(this);
        if (savedInstanceState != null) {
            listScrollState = savedInstanceState.getParcelable(KEY_LIST_SCROLL_STATE);
        }
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapter);
        presenter.bindView(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(KEY_LIST_SCROLL_STATE, recyclerView.getLayoutManager().onSaveInstanceState());
    }

    @Override
    public void setNews(List<NewsItem> news, boolean refresh) {
        adapter.add(news, refresh);
        if (listScrollState != null) {
            recyclerView.getLayoutManager().onRestoreInstanceState(listScrollState);
            listScrollState = null;
        }
    }

    @Override
    public Observable<Object> refreshEvent() {
        return RxSwipeRefreshLayout.refreshes(swipeRefreshLayout);
    }

    @Override
    public Observable<RecyclerViewScrollEvent> loadMoreEvent() {
        return RxRecyclerView.scrollEvents(recyclerView)
                .debounce(50, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .filter(event -> {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) event.view().getLayoutManager();
                    int lastVisible = layoutManager.findLastVisibleItemPosition();
                    int totalItems = layoutManager.getItemCount();
                    if (swipeRefreshLayout.isRefreshing() && previousTotalItems < totalItems) {
                        swipeRefreshLayout.setRefreshing(false);
                        previousTotalItems = totalItems;
                        return false;
                    }

                    if (!swipeRefreshLayout.isRefreshing() && lastVisible + LOADING_THRESHOLD > totalItems) {
                        swipeRefreshLayout.setRefreshing(true);
                        return true;
                    }
                    return false;
                });
    }

    @Override
    public boolean hasPendingScrollState() {
        return listScrollState != null;
    }

    @Override
    public void showError(String message) {
        if (message == null) message = getString(R.string.plain_error);
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public Observable<NewsItem> itemClickEvent() {
        return adapter.getClickSubject();
    }

    @Override
    public void setLoading(boolean loading) {
        swipeRefreshLayout.setRefreshing(loading);
    }

    public void goToImageScreen(String imageUrl) {
        ImageViewActivity.start(this, imageUrl);
    }

    @Override
    protected void onDestroy() {
        presenter.destroyView();
        super.onDestroy();
    }
}
