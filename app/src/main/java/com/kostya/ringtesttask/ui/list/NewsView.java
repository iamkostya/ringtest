package com.kostya.ringtesttask.ui.list;

import com.jakewharton.rxbinding2.support.v7.widget.RecyclerViewScrollEvent;
import com.kostya.ringtesttask.data.entity.NewsItem;
import com.kostya.ringtesttask.ui.View;

import java.util.List;

import io.reactivex.Observable;

public interface NewsView extends View {

    void setNews(List<NewsItem> news, boolean refresh);
    void goToImageScreen(String imageUrl);
    boolean hasPendingScrollState();

    Observable<Object> refreshEvent();
    Observable<RecyclerViewScrollEvent> loadMoreEvent();
    Observable<NewsItem> itemClickEvent();
}
