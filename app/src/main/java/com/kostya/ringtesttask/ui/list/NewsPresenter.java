package com.kostya.ringtesttask.ui.list;

import android.text.TextUtils;
import android.util.Log;

import com.jakewharton.rxbinding2.support.v7.widget.RecyclerViewScrollEvent;
import com.kostya.ringtesttask.data.entity.NewsItem;
import com.kostya.ringtesttask.domain.NewsInteractor;
import com.kostya.ringtesttask.ui.Presenter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;

public class NewsPresenter extends Presenter<NewsView> {

    private NewsInteractor interactor;

    @Inject
    public NewsPresenter(NewsInteractor interactor) {
        this.interactor = interactor;
    }

    @Override
    public void bindView(NewsView view) {
        super.bindView(view);
        fetchNews(true).subscribe(getNewsConsumer(true));

        compositeDisposable.add(view.loadMoreEvent()
                .flatMapSingle(it -> fetchNews(false))
                .doOnError(error -> view.showError(error.getMessage()))
                .retry()
                .subscribe(getNewsConsumer(false), getErrorConsumer()));

        compositeDisposable.add(view.refreshEvent()
                .flatMapSingle((it -> fetchNews(true)))
                .subscribe(getNewsConsumer(true), getErrorConsumer()));

        compositeDisposable.add(view.itemClickEvent()
                .subscribe(this::handleItemClick, getErrorConsumer()));
    }

    private void handleItemClick(NewsItem newsItem) {
        if (view != null) {
            if (newsItem.getPreview().getImages().get(0).getVariants().getGif() != null &&
                    !TextUtils.isEmpty(newsItem.getPreview().getImages().get(0).getVariants().getGif()
                            .getSources().getUrl())) {
                view.goToImageScreen(newsItem.getPreview().getImages().get(0).getVariants().getGif()
                        .getSources().getUrl());
            } else if (!TextUtils.isEmpty(newsItem.getPreview().getImages().get(0).getSources().getUrl())) {
                view.goToImageScreen(newsItem.getPreview().getImages().get(0).getSources().getUrl());
            }
        }
    }

    private Single<List<NewsItem>> fetchNews(boolean refresh) {
        Single<List<NewsItem>> source;
        if (view.hasPendingScrollState()) {
            source = interactor.getCachedNews();
        } else {
            source = interactor.loadMoreNews(refresh);
        }
        return source
                .doOnSubscribe(disposable -> {
                    if (view != null) view.setLoading(true);
                })
                .doOnSuccess(disposable -> {
                    if (view != null) view.setLoading(false);
                })
                .doOnError(disposable -> {
                    if (view != null) view.setLoading(false);
                })
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Consumer<List<NewsItem>> getNewsConsumer(boolean refresh) {
        return list -> {
            if (view != null) {
                view.setNews(list, refresh);
            }
        };
    }

    private Consumer<Throwable> getErrorConsumer() {
        return error -> {
            Log.d(TAG, error.getMessage());
            if (view != null) {
                view.showError(error.getMessage());
            }
        };
    }
}
