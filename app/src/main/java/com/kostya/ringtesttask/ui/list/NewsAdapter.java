package com.kostya.ringtesttask.ui.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.kostya.ringtesttask.R;
import com.kostya.ringtesttask.data.entity.NewsItem;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.subjects.PublishSubject;

public class NewsAdapter extends RecyclerView.Adapter<NewsItemViewHolder> {

    private List<NewsItem> news = new ArrayList<>();
    private LayoutInflater inflater;
    private PublishSubject<NewsItem> clickSubject = PublishSubject.create();

    public void add(List<NewsItem> list, boolean refresh) {
        if (refresh) {
            news.clear();
        }
        this.news.addAll(list);
        notifyDataSetChanged();
    }

    public PublishSubject<NewsItem> getClickSubject() {
        return clickSubject;
    }

    @Override
    public NewsItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        return new NewsItemViewHolder(inflater.inflate(R.layout.news_item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(NewsItemViewHolder holder, int position) {
        holder.bind(news.get(position), clickSubject);
    }

    @Override
    public int getItemCount() {
        return news.size();
    }
}
