package com.kostya.ringtesttask.ui;

import android.support.annotation.Nullable;

import io.reactivex.disposables.CompositeDisposable;

public abstract class Presenter<T> {

    protected final String TAG = this.getClass().getSimpleName();

    protected CompositeDisposable compositeDisposable;

    @Nullable
    protected T view;

    public void bindView(T view) {
        this.view = view;
        compositeDisposable = new CompositeDisposable();
    }

    public void unbindView() {
        if (view != null) view = null;
    }

    public void start() {

    }

    public void stop() {

    }

    public void destroyView() {
        unbindView();
        compositeDisposable.clear();
        compositeDisposable = null;
    }
}
