package com.kostya.ringtesttask.ui.image;

import android.util.Log;

import com.kostya.ringtesttask.domain.ImageDownloader;
import com.kostya.ringtesttask.ui.Presenter;

import javax.inject.Inject;

public class ImagePresenter extends Presenter<ImageViewContract> {

    private ImageDownloader imageDownloader;

    @Inject
    public ImagePresenter(ImageDownloader imageDownloader) {
        this.imageDownloader = imageDownloader;
    }

    @Override
    public void bindView(ImageViewContract view) {
        super.bindView(view);
        if (view != null) {
            compositeDisposable.add(view.imageLoadingEvent()
                    .subscribe(event -> {
                        if (event == ImageViewContract.ImageLoadingEvent.FINISHED) {
                            view.setLoading(false);
                        } else {
                            view.setLoading(false);
                            view.showError("Can't load image preview");
                        }
                    }, e -> {
                        view.showError(e.getMessage());
                        Log.d(TAG, e.getMessage());
                    }));

            compositeDisposable.add(view.saveEvent()
                    .subscribe(url -> {
                        imageDownloader.download(url);
                    }, e -> {
                        view.showError(e.getMessage());
                        Log.d(TAG, e.getMessage());
                    }));
            view.setLoading(true);
        }
    }
}
