package com.kostya.ringtesttask;

import android.app.Application;

import com.kostya.ringtesttask.di.DaggerDataComponent;
import com.kostya.ringtesttask.di.DataComponent;
import com.kostya.ringtesttask.di.DataModule;

public class App extends Application {

    DataComponent dataComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        dataComponent = DaggerDataComponent.builder()
                .dataModule(new DataModule(this))
                .build();
    }

    public DataComponent getDataComponent() {
        return dataComponent;
    }
}
